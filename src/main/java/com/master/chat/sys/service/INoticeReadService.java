package com.master.chat.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.master.chat.sys.pojo.entity.NoticeRead;

/**
 * 系统通知已读状态 服务类
 *
 * @author: Yang
 * @date: ${date}
 * @version: 1.0.0
 * Copyright Ⓒ 2023 Master Computer Corporation Limited All rights reserved.
 */
public interface INoticeReadService extends IService<NoticeRead> {

}
