package com.master.chat.common.enums;

/**
 * 操作人类别
 *
 * @author: Yang
 * @date: 2021/10/20
 * @version: 1.2.0
 * Copyright Ⓒ 2023 Master Computer Corporation Limited All rights reserved.
 */
public enum OperatorTypeEnum {

    /**
     * 后台用户
     */
    MANAGE,

    /**
     * 手机端用户
     */
    MOBILE,

    /**
     * 其它
     */
    OTHER

}
