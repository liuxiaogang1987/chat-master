package com.master.chat.api.moonshot.constant;

/**
 * 月之暗面模型版本
 *
 * @author: Yang
 * @date: 2024/3/26
 * @version: 1.2.0
 * Copyright Ⓒ 2023 Master Computer Corporation Limited All rights reserved.
 */
public interface MoonshotApiVersion {

    String API_MODEL_8K = "moonshot-v1-8k";

}
